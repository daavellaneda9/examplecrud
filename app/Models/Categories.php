<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    public function state(){
    	return $this->hasMany('App\Models\State');
    }

    public function movie(){
		return $this->belongsToMany('App\Models\Movie');
	}
}
