<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
	public function user(){
		return $this->belongsTo('App\User');
	}

	public function movie(){
		return $this->belongsTo('App\Models\Movie');
	}

	public function categories(){
		return $this->belongsTo('App\Models\categories');
	}
}
