<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie as Movie;
use App\Models\Categories as Categories;
use App\Models\States as States;


class MovieController extends Controller
{

    public function index(){
        $movies=Movie::all();
        
        return \View::make('movies/list',compact('movies'));
    }

    public function create(){
        $categories=Categories::all();
        return \View::make('movies/new',compact('categories'));
    }

    public function store(Request $request){
    	dd($request->listCategories);
        //$movie=$this->saveMovie($request);
        //$this->saveMovieCategory($request->listCategories,$movie);
        //return redirect('movie');
    }

    public function saveMovie($request){
        $movie= new Movie;
        $movie->name = $request->name;
        $movie->description = $request->description;
        $movie->user_id= \Auth::user()->id;
        $movie->state_id=1;
        $movie->save();
        return $movie;
    }

    public function saveMovieCategory($listCategories,$movie){
        foreach ($listCategories as $category) {
            $movie->categories()->attach(
                $category['id'],
                ['state_id'=>1]
            );
        }
    }

    public function edit($id){
        $movie= Movie::find($id);
        return \View::make('movies/update',compact('movie'));
    }

    public function update($id, Request $request){
        $movie= Movie::find($id);
        $movie->name=$request->name;
        $movie->description=$request->description;
        $movie->save();
        return redirect('movie');
    }

    public function show(Request $request){
        $movies=Movie::where('name','like','%'.$request->name.'%')->get();
        return \View::make('movies/list',compact('movies'));
    }


    public function destroy($id){
        $movie= Movie::find($id);
        $movie->delete();
        return redirect()->back();
    }

}
