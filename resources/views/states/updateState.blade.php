@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!!Form::model($states,['route'=>['state.update',$states->id],'method'=>'put','novalidate'])!!}
				<div class="form-group">
					<label>Estado</label>
					<input type="text" name="state" class="form-control" required value="{{$states->state}}">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-success" >Editar</button>
				</div>
				{!!Form::close()!!}
			</article>
		</div>
	</section>
@endsection