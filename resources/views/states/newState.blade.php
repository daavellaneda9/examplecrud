@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!!Form::open(['route'=>'state.store','method'=>'post','novalidate'])!!}
				<div class="form-group">
					<label>Estado</label>
					<input type="text" name="state" class="form-control">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-success" >Crear</button>
				</div>
				{!!Form::close()!!}
			</article>
		</div>
	</section>
@endsection