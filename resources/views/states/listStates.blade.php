@extends('layouts.app')
@section('content')
 <section class="container">
 	<div class="row">
 		<article class="col-md-12">
 			{!!Form::open(['route'=>'state/show', 'method'=>'post', 'novalidate', 'class'=>'form-inline'])!!}
 			<div class="form-group">
 				<label>Nombre</label>
 				<input type="text" name="name" class="form-control">
 			</div>
 			<div class="form-group">
 				<button type="submit" class="btn btn-default" >Buscar</button>
 				<a href="{{route('state.index')}}" class="btn btn-primary">Todos</a>
 				<a href="{{route('state.create')}}" class="btn btn-primary">Crear</a>
 			</div>
 			{!!Form::close()!!}
 		</article>
 		<article class="col-md-12">
 			<table class="table table-condensed table-striped table-bordered">
 				<thead>
 					<tr>
 						<th>Estado</th>
 					</tr>
 				</thead>
 				<tbody>
 					@foreach($states as $state)
 					<tr>
 						<td>{{$state->state}}</td>
 						<td>        
 							<a href="{{route('state.edit',['id'=>$state->id])}}" class="btn btn-primary xs">Editar</a>
 							<a href="{{route('state/destroy',['id'=>$state->id])}}" class="btn btn-danger xs">Eliminar</a>
 						</td>
 					</tr>
 					@endforeach
 				</tbody>
 			</table>
 		</article>
 	</div>
 </section>
@endsection