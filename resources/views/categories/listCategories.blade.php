@extends('layouts.app')
@section('content')

	<section class="container">
		<article class="col-md-12">
			{!!Form::open(['route'=>'category/show','method'=>'post','novalidate','class'=>'form-inline'])!!}
				<div class="form-group">
					<label>Nombre</label>
					<input type="text" class="form-control" name="name">
				</div>
				<div class="form-control">
					<button type="submit" class="btn btn-default">Buscar</button>
					<a href="{{ route('category.index') }}" class="btn btn-primary">Todas</a>
					<a href="{{ route('category.create') }}" class="btn btn-primary">Crear</a>
				</div>
			{!!Form::close()!!}
		</article>
		<article class="col-md-12">
			<table class="table table-condensed table-striped table-bordered">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Acción</th>
					</tr>
				</thead>
				<tbody>
					@foreach($categories as $category)
					<tr>
						<td>{{ $category->name}}</td>
						<td>
							<a class="btn btn-primary btn-xs" href="{{route('category.edit',['id'=>$category->id])}}">Editar</a>
							<a class="btn btn-danger btn-xs" href="{{route('category/destroy',['id'=>$category->id])}}">Eliminar</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</article>
	</section>
@endsection