@extends('layouts.app')
@section('content')
	
	<section class="container">
		<article>
			{!!Form::open(['route'=>'category.store','method'=>'post','novalidate'])!!}
			<div class="form-group">
				<label>Nombre de la categoria</label>
				<input type="text" name="name" class="form-control" required>
			</div>
			<div>
				<button type="submit" class="btn btn-success" >Registrar</button>
			</div>
			{!!Form::close()!!}
		</article>
	</section>

@endsection