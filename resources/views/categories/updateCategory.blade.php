@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!!Form::model($categories,['route'=>['category.update',$categories->id],'method'=>'put','novalidate'])!!}
				<div class="form-group">
					<label>Nombre</label>
					<input type="text" name="name" class="form-control" required value="{{$categories->name}}">
				</div>
				<div class="form-group">
					<label>Estado</label>
					<input type="text" name="state_id" class="form-control" required value="{{$categories->state_id}}">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-success" >Editar</button>
				</div>
				{!!Form::close()!!}
			</article>
		</div>
	</section>
@endsection