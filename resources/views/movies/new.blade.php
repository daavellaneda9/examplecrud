@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!!Form::open(['route'=>'movie.store', 'method'=>'post','novalidate']) !!}
				<div class="form-group">
					<label>Nombre</label>
					<input type="text" name="name" class="form-control" required>
				</div>
				<div class="form-group">
					<label>Descripción</label>
					<input type="text" name="description" class="form-control" required>
				</div>
				<div class="form-group">
					<label>Categoria</label>
					<select id="category" >
						@foreach($categories as $category)
						<option value="{{$category->name}}">{{$category->name}}</option>
						@endforeach
					</select>
					<button type="button" onclick="agregar()">Agregar</button>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-success"> Enviar</button> 
				</div>
				<input type="hidden" id="listCategories" name="listCategories[]">
				{!! Form::close() !!}
			</article>
			<script type="text/javascript">
				function agregar(){
					var category=document.getElementById('category').value;
					var categories= Array();
					var lista=document.getElementById('listCategories');

					categories.push(category);
					lista.append(category);
				}
			</script>
		</div>
	</section>
@endsection