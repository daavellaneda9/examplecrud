@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!!Form::open(['route'=>'user/show','method'=>'post','novalidate','class'=>'form-inline'])!!}
				<div class="form-group">
					<label>Nombre</label>
					<input type="text" name="name" class="form-control">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-default">Buscar</button>
					<a href="{{route('user.index')}}" class="btn btn-primary">Todos</a>
					<a href="{{route('user.create')}}" class="btn btn-primary">Crear</a>
				</div>
				{!!Form::close()!!}
			</article>
			<article class="col-md-10">
				<table class="table table-condensed table-striped table-bordered">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Estado</th>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $user)
						<tr>
							<td>{{$user->name}}</td>
							<td>{{$user->state_id}}</td>
							<td>
								<a href="{{route('user.edit',['id'=>$user->id])}}" class="btn btn-primary xs">Editar</a>
								<a href="{{route('user/destroy',['id'=>$user->id])}}" class="btn btn-danger xs">Eliminar</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</article>
		</div>
	</section>
@endsection