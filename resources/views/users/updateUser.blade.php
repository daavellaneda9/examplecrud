@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!!Form::model($users,['route'=>['user.update',$users->id],'method'=>'put','novalidate'])!!}
				<div class="form-group">
					<label>Nombre</label>
					<input type="text" name="name" required value="{{$users->name}}" class="form-control">
				</div>
				<div class="form-group">
					<label>Estado</label>
					<input type="text" name="state_id" required value="{{$users->state_id}}" class="form-control">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Editar</button>
				</div>
				{!!Form::close()!!}
			</article>
		</div>
	</section>
@endsection