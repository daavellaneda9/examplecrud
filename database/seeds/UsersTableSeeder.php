<?php

use Illuminate\Database\Seeder;

//CLass Faker para crear datos aleatorios
use Faker\Factory as Faker;
//Class User para crear nuevos usuarios
use App\User;
//Facade Hash para crear claves encriptadas
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker::create();
        for ($i=0; $i < 10 ; $i++) { 
        	$user= new User;
        	$user->name=$faker->name;
        	$user->email=$faker->email;
        	$user->password=Hash::make('test123');
        	$user->state_id=1;
            $NumberRol=random_int(1, 2);
            if($NumberRol===1){
                $user->rol="Administrator";
            }else{
                $user->rol="User";
            }
        	$user->save();
        }
    }
}
