<?php

use Illuminate\Database\Seeder;
//Class User para crear uevos estados
use App\Models\States;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = array(['state'=>'Activo'],['state'=>'Inactivo']);

        foreach ($states as $value) {
        	$state= new States;
        	$state->state= $value['state'];
        	$state->save();
        }
    }
}
