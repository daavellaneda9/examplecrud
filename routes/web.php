<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('movie','MovieController');
Route::get('movie/destroy/{id}',['as'=>'movie/destroy','uses'=>'MovieController@destroy']);
Route::post('movie/show',['as'=>'movie/show','uses'=>'MovieController@show']);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
///////////////////////////////////////////////////////
Route::resource('category','CategoryController');
Route::post('category/show',['as'=>'category/show','uses'=>'CategoryController@show']);
Route::get('category/destroy/{id}',['as'=>'category/destroy','uses'=>'CategoryController@destroy']);

//////////////////////////////////////////////////////
Route::resource('state','StateController');
Route::post('state/show',['as'=>'state/show','uses'=>'StateController@show']);
Route::get('state/destroy/{id}',['as'=>'state/destroy','uses'=>'StateController@destroy']);

//////////////////////////////////////////////////////
Route::resource('user','UserController');
Route::post('user/show',['as'=>'user/show','uses'=>'UserController@show']);
Route::get('user/destroy/{id}',['as'=>'user/destroy','uses'=>'UserController@destroy']);